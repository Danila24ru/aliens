﻿using System;
using System.Threading;
using System.Threading.Tasks;

public abstract class Ability
{
    protected Alien owner;

    public void SetOwner(Alien owner) => this.owner = owner;

    public virtual Task Execute(CancellationToken token) { return Task.CompletedTask; }
}
