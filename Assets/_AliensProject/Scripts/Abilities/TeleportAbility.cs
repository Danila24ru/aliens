﻿using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

public class TeleportAbility : Ability
{
    private readonly float radius;

    public TeleportAbility(float radius) 
    {
        this.radius = radius;
    }

    public override async Task Execute(CancellationToken token)
    {
        owner.MoveToPosition(owner.GetView().transform.position + Vector3.up * radius);
    }
}
