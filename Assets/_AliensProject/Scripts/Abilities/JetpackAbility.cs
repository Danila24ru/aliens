﻿using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

public class JetpackAbility : Ability
{
    private readonly float duration;

    public JetpackAbility(float duration)
    {
        this.duration = duration;
    }

    public override async Task Execute(CancellationToken token)
    {
        owner.SetVelocity(Vector3.up * 10);
    }
}