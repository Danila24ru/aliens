﻿using UnityEngine;

public class AlienView : MonoBehaviour
{
    [SerializeField] private Transform viewOrigin;
    [SerializeField] private Transform weaponOrigin;

    [SerializeField] private Rigidbody body;

    private Alien alienOwner;

    public void SetAlienOwner(Alien alienOwner)
    {
        this.alienOwner = alienOwner;
    }

    public Alien GetAlienOwner() => alienOwner;

    public Transform GetWeaponOrigin() => weaponOrigin;
    public Rigidbody GetRigidbody() => body;
}
