using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

public class Alien : IDisposable
{
    private string alienFamily;
    private AlienView alienView;
    private Weapon weapon;
    private Ability ability;

    private StateMachine<AlienBlackboard> brain;

    public Alien(string alienFamily, AlienView alienViewGo)
    {
        this.alienFamily = alienFamily;
        this.alienView = alienViewGo;

        alienViewGo.SetAlienOwner(this);
    }

    public string AlienFamily => alienFamily;
    public AlienView GetView() => alienView;

    public void SetWeapon(Weapon weapon)
    {
        this.weapon = weapon;
        weapon.SetOwner(this);
    }

    public void SetAbility(Ability ability)
    {
        this.ability = ability;
        ability.SetOwner(this);
    }

    public void SetBrain(StateMachine<AlienBlackboard> brain)
    {
        this.brain = brain;
    }

    public StateMachine<AlienBlackboard> GetBrain() => brain;

    public virtual async Task Attack(CancellationToken token)
    { 
        if(weapon == null)
        {
            return;
        }

        await weapon.Fire(token);
    }


    public virtual async Task UseAbility(CancellationToken token)
    {
        if (ability == null)
        {
            return;
        }

        await ability.Execute(token);
    }

    public virtual void MoveToPosition(Vector3 position) 
    {
        alienView.GetRigidbody().MovePosition(position);
    }

    public virtual void SetVelocity(Vector3 velocity)
    {
        alienView.GetRigidbody().velocity = velocity;
    }

    public Vector3 GetVelocity() => alienView.GetRigidbody().velocity;

    public void Dispose()
    {
        GameObject.Destroy(alienView.gameObject);
    }
}
