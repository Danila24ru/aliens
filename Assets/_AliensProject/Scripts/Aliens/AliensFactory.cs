﻿using System.Threading;
using UnityEngine;

public class AliensFactory
{
    private readonly CancellationToken globalToken;
    private readonly AliensSettingsSO aliensSettings;
    private readonly ViewsContainerSO viewsContainer;

    public AliensFactory(CancellationToken globalToken, AliensSettingsSO aliensSettings, ViewsContainerSO viewsContainer)
    {
        this.globalToken = globalToken;
        this.aliensSettings = aliensSettings;
        this.viewsContainer = viewsContainer;
    }

    public Alien CreateAlienA(Vector3 position)
    {
        var family = "Family_A";
        var alienView = aliensSettings.GetAlienView(family);
        alienView.transform.position = position;

        var weapon = new ProjectileWeapon(viewsContainer.GetProjectileView(0), fireRate: 0.5f);
        var ability = new TeleportAbility(1.0f);
        
        return CreateCustomAlien(family, alienView, weapon, ability);
    }

    public Alien CreateAlienB(Vector3 position)
    {
        var family = "Family_B";
        var alienView = aliensSettings.GetAlienView(family);
        alienView.transform.position = position;

        var weapon = new RayWeapon(viewsContainer.GetRayView(0), fireRate: 0.1f);
        var ability = new JetpackAbility(5.0f);
    
        return CreateCustomAlien(family, alienView, weapon, ability);
    }

    public Alien CreateCustomAlien(string family, AlienView alienViewPrefab, Weapon weapon, Ability ability)
    {
        var alienViewGo = GameObject.Instantiate(alienViewPrefab);
        var alien = new Alien(family, alienViewGo);
        var brain = CreateAlienBrain(alien);
        alien.SetBrain(brain);
        alien.SetWeapon(weapon);
        alien.SetAbility(ability);
        return alien;
    }

    public StateMachine<AlienBlackboard> CreateAlienBrain(Alien alien)
    {
        var sm = new StateMachine<AlienBlackboard>(new AlienBlackboard()
        {
            owner = alien
        });

        var movingState = new MoveAlienState("MoveAlien");
        var movingState2 = new MoveAlienState("MoveAlien2");
        var attackState = new AttackState("AttackState");
        var useAbilityState = new UseAbilityState("UseAbilityState");

        sm.AddState(movingState, movingState2, attackState, useAbilityState);

        var waitTimeDecision = new WaitRandomTimeDecision(2.0f, 8.0f);
        var exitDecision = new ImmediateExitDecision();

        var movingToAttackTransition = sm.CreateTransition(movingState, attackState, waitTimeDecision);
        var attackToMoving2Transition = sm.CreateTransition(attackState, movingState2, exitDecision);
        var moving2ToAbilityTransition = sm.CreateTransition(movingState2, useAbilityState, waitTimeDecision);
        var useAbilityToMovingTransition = sm.CreateTransition(useAbilityState, movingState, exitDecision);

        movingState.transitions.Add(movingToAttackTransition);
        attackState.transitions.Add(attackToMoving2Transition);
        movingState2.transitions.Add(moving2ToAbilityTransition);
        useAbilityState.transitions.Add(useAbilityToMovingTransition);

        sm.SetCurrentState(movingState);

        sm.InitMachine(globalToken);

        return sm;
    }
}
