﻿using Project.Addressable;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;

public class Game : IDisposable
{
    private readonly AliensFactory alienFactory;
    private readonly GameSettings gameSettings;

    private readonly AddressablesProvider addressablesProvider;

    private List<Alien> aliens = new List<Alien>();

    private CancellationTokenSource globalCts;

    public Game(GameSettings gameSettings, AliensSettingsSO aliensSettingsSO, ViewsContainerSO viewsContainerSO)
    {
        this.gameSettings = gameSettings;

        globalCts = new CancellationTokenSource();
        
        addressablesProvider = ServiceLocator.Instance.GetService<AddressablesProvider>();

        alienFactory = new AliensFactory(globalCts.Token, aliensSettingsSO, viewsContainerSO);

        MessageBroker.Instance.Subscribe<HitEvent>(OnProjectileHit);
    }

    public void StartGame()
    {
        var alienA = alienFactory.CreateAlienA(gameSettings.spawnA.position);
        var alienB = alienFactory.CreateAlienB(gameSettings.spawnB.position);

        aliens.Add(alienA);
        aliens.Add(alienB);
    }

    private void OnProjectileHit(HitEvent hitEvent)
    {
        if(hitEvent.hitOwner == hitEvent.hitTarget)
        {
            return;
        }

        //UnityEngine.Debug.Log($"Hit: {hitEvent.hitOwner.gameObject.name} -> {hitEvent.hitTarget.gameObject.name}");
        //aliens.Remove();
    }

    public void Update(float deltaTime)
    {
    }

    public void Dispose()
    {
        MessageBroker.Instance.Unsubscribe<HitEvent>(OnProjectileHit);
        globalCts.Cancel();
    }
}