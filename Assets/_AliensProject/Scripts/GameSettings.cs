﻿using System;
using UnityEngine;

[Serializable]
public class GameSettings
{
    public int maxSpawnedAliensA = 10;
    public int maxSpawnedAliensB = 10;

    public Transform spawnA;
    public Transform spawnB;
}
