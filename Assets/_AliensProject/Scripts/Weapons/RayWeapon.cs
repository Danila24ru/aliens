﻿using System.Threading;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Pool;

public class RayWeapon : Weapon
{
    protected readonly ObjectPool<RayView> viewPool;
    private readonly float distance;
    private readonly float rayDuration;

    public RayWeapon(RayView view, float fireRate = 0.0f, float distance = 100.0f, float rayDuration = 3.0f) : base(fireRate)
    {
        viewPool = new ObjectPool<RayView>(
            createFunc: () => { return GameObject.Instantiate(view); },
            actionOnGet: (view) => { view.gameObject.SetActive(true); },
            actionOnRelease: (view) => { view.gameObject.SetActive(false); });

        this.distance = distance;
        this.rayDuration = rayDuration;
    }

    protected async override Task FireLogic(CancellationToken token)
    {
        var view = viewPool.Get();

        view.transform.position = weaponOrigin.position;

        var t = 0.0f;
        while (t < rayDuration)
        {
            if (token.IsCancellationRequested)
                break;

            var origin = weaponOrigin.position;
            var direction = weaponOrigin.right * 3;

            view.SetRay(origin, direction);
            t += Time.deltaTime;

            var ray = new Ray(origin, direction * distance);
            RaycastHit hit;
            if(Physics.Raycast(ray, out hit))
            {
                MessageBroker.Instance.Publish(new HitEvent()
                {
                    hitOwner = owner.GetView().GetComponent<Collider>(),
                    hitTarget = hit.collider
                });
                break;
            }

            await Task.Yield();
        }

        viewPool.Release(view);
    }
}
