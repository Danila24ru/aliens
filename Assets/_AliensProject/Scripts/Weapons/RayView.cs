﻿using UnityEngine;

public class RayView : MonoBehaviour
{
    [SerializeField] private LineRenderer lineRenderer;

    public void SetRay(Vector3 start, Vector3 end)
    {
        lineRenderer.SetPositions(new Vector3[] { start, end });
    }
}
