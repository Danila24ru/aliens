﻿using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

public abstract class Weapon
{
    protected readonly float fireRate;
    protected float nextFireTime;
    protected Transform weaponOrigin;
    protected Alien owner;

    public Weapon(float fireRate)
    {
        this.fireRate = fireRate;
    }

    public void SetOwner(Alien owner)
    {
        this.owner = owner;

        SetWeaponOrigin(owner.GetView().GetWeaponOrigin());
    }

    private void SetWeaponOrigin(Transform weaponOrigin)
    {
        this.weaponOrigin = weaponOrigin;
    }

    public async Task Fire(CancellationToken token) 
    {
        if (CanFire())
        {
            await FireLogic(token);
        }
    }

    protected virtual Task FireLogic(CancellationToken token)
    {
        return Task.CompletedTask;
    }

    protected virtual bool CanFire() 
    {
        if(Time.time > nextFireTime)
        {
            nextFireTime = Time.time + fireRate;
            return true;
        }
        return false;
    }
}
