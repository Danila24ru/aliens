﻿using System;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Pool;

public class ProjectileWeapon : Weapon
{
    protected readonly ObjectPool<ProjectileView> viewPool;

    public ProjectileWeapon(ProjectileView view, float fireRate = 0.5f) : base(fireRate)
    {
        viewPool = new ObjectPool<ProjectileView>(
            createFunc: () => { return GameObject.Instantiate(view); },
            actionOnGet: (view) => { view.gameObject.SetActive(true); },
            actionOnRelease: (view) => { view.gameObject.SetActive(false); });
    }

    protected async override Task FireLogic(CancellationToken token)
    {
        var view = viewPool.Get();

        view.transform.position = weaponOrigin.position;
        view.SetVelocity(Vector3.right * 10.0f);

        var delayTask = Task.Delay(500, token);
        try
        {
            await delayTask;
        }
        catch
        {
            return;
        }

        var projectileAliveTask = view.Alive(token);

        if(token.IsCancellationRequested)
        {
            return;
        }

        await Task.WhenAny(delayTask, projectileAliveTask);

        viewPool.Release(view);
    }
}
