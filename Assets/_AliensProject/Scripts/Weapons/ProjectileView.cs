﻿using System;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

public class HitEvent
{
    public Collider hitOwner;
    public Collider hitTarget;
}

public class ProjectileView : MonoBehaviour
{
    [SerializeField] private Rigidbody body;

    private bool isHitSomething;

    private void OnEnable()
    {
        isHitSomething = false;
    }

    public async Task Alive(CancellationToken token)
    {
        while (!isHitSomething && !token.IsCancellationRequested)
        {
            await Task.Yield();
        }
    }

    public void OnCollisionEnter(Collision collision)
    {
        MessageBroker.Instance.Publish(new HitEvent() 
        {
            hitOwner = GetComponent<Collider>(),
            hitTarget = collision.collider,
        });

        isHitSomething = true;
    }

    public void SetVelocity(Vector3 velocity)
    {
        body.velocity = velocity;
    }
}
