
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

public abstract class Blackboard { }

public class StateMachine<T> where T : Blackboard
{
    private readonly List<State<T>> states = new List<State<T>>();
    private readonly T blackboard;
    private State<T> currentState;

    public StateMachine(T blackboard) 
    {
        this.blackboard = blackboard;
    }

    public void SetCurrentState(State<T> state)
    {
        if(!states.Contains(state))
        {
            states.Add(state);
        }

        currentState = state;
    }
    public void AddState(params State<T>[] newStates)
    {
        states.AddRange(newStates);
    }

    public Transition<T> CreateTransition(State<T> stateA, State<T> stateB, Decision<T> decision)
    {
        return new Transition<T>(stateA, stateB, decision);
    }

    public async void InitMachine(CancellationToken token)
    {
        if (states == null || states.Count == 0)
        {
            return;
        }

        foreach (var state in states)
        {
            state.SetBlackboard(blackboard);

            foreach (var transition in state.transitions)
            {
                transition.decision.SetBlackboard(blackboard);    
            }
        }

        currentState = states[0];
        await currentState.OnEnter(token);
        if (token.IsCancellationRequested)
        {
            return;
        }
            
        foreach(var decision in currentState.transitions.Select(x => x.decision))
        {
            decision.OnEnter();
        }

        await RunMachine(token);
    }

    private async Task RunMachine(CancellationToken token)
    {
        if(currentState == null)
        {
            return;
        }

        while (!token.IsCancellationRequested)
        {
            currentState.OnUpdate();

            foreach (var transition in currentState.transitions)
            {
                if (transition.decision.OnUpdate())
                {
                    transition.decision.OnExit();

                    await transition.stateA.OnExit(token);
                    if (token.IsCancellationRequested)
                        return;

                    if (transition.stateB == null)
                    {
                        UnityEngine.Debug.Log("exit state machine");
                        return;
                    }

                    await transition.stateB.OnEnter(token);
                    if (token.IsCancellationRequested)
                        return;

                    foreach (var bTransitions in transition.stateB.transitions)
                    {
                        bTransitions.decision.OnEnter();
                    }

                    currentState = transition.stateB;
                    break;
                }
            }

            await Task.Yield();
        }
    }
}

public class State<T>
{
    public readonly string Name;

    protected T blackboard;
    public void SetBlackboard(T blackboard) => this.blackboard = blackboard;

    public List<Transition<T>> transitions;

    public State(string name)
    {
        this.Name = name;
        transitions = new List<Transition<T>>();
    }

    public virtual Task OnEnter(CancellationToken token) { return Task.CompletedTask; }
    public virtual void OnUpdate() { }
    public virtual Task OnExit(CancellationToken token) { return Task.CompletedTask; }
}

public class Transition<T>
{
    public State<T> stateA;
    public State<T> stateB;
    public Decision<T> decision;

    public Transition(State<T> stateA, State<T> stateB, Decision<T> decision)
    {
        this.stateA = stateA;
        this.stateB = stateB;
        this.decision = decision;
    }
}

public abstract class Decision<T>
{
    protected T blackboard;
    public void SetBlackboard(T blackboard) => this.blackboard = blackboard;
    public abstract void OnEnter();
    public abstract bool OnUpdate();
    public abstract void OnExit();
}