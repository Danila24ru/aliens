﻿using System.Threading;
using System.Threading.Tasks;

public class AttackState : State<AlienBlackboard>
{
    public AttackState(string name) : base(name) { }
    public override async Task OnEnter(CancellationToken token)
    {
        await blackboard.owner.Attack(token);
    }
}
