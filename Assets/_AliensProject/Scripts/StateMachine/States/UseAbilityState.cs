﻿using System.Threading;
using System.Threading.Tasks;

public class UseAbilityState : State<AlienBlackboard>
{
    public UseAbilityState(string name) : base(name) { }

    public override async Task OnEnter(CancellationToken token)
    {
        await blackboard.owner.UseAbility(token);
    }
}
