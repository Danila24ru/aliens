﻿using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

public class MoveAlienState : State<AlienBlackboard>
{
    private float moveDirectionSign;
    private float velocity = 5.0f;

    public MoveAlienState(string name) : base(name) 
    {
        moveDirectionSign = Mathf.Sign(Random.value - 0.5f);
    }

    public override void OnUpdate()
    {
        var currentVelocity = blackboard.owner.GetVelocity();

        blackboard.owner.SetVelocity(new Vector3(moveDirectionSign * velocity, currentVelocity.y, currentVelocity.z));

        var origin = blackboard.owner.GetView().GetWeaponOrigin();

        if ((int)Time.time % 3 == 0) // хаотичный мувмент
        {
            moveDirectionSign *= -1.0f;
        }
    }
}
