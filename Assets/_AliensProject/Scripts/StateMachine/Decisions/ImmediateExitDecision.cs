﻿using System.Threading;
using System.Threading.Tasks;

public class ImmediateExitDecision : Decision<AlienBlackboard>
{
    public override void OnEnter()
    {
    }

    public override void OnExit()
    {
    }

    public override bool OnUpdate()
    {
        return true;
    }
}
