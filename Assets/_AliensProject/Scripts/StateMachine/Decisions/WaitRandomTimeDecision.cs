﻿using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

public class WaitRandomTimeDecision : Decision<AlienBlackboard>
{
    private float timer;
    private float maxMoveTime;

    private readonly float minTime;
    private readonly float maxTime;

    public WaitRandomTimeDecision(float minTime, float maxTime)
    {
        this.minTime = minTime;
        this.maxTime = maxTime;
    }

    public override void OnEnter()
    {
        timer = 0.0f;
        maxMoveTime = Random.Range(minTime, maxTime);
    }

    public override void OnExit()
    {
    }

    public override bool OnUpdate()
    {
        timer += Time.deltaTime;
        return timer >= maxMoveTime;
    }
}
