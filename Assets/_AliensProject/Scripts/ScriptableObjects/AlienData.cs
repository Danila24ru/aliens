﻿using System;

[Serializable]
public class AlienData
{
    public string alienFamily;
    public AlienView alienView;
}
