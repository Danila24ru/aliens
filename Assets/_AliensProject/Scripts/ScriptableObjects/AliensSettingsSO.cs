﻿using System.Linq;
using UnityEngine;

[CreateAssetMenu(fileName = "AliensSettings", menuName = "Scriptables/AliensSettings")]
public class AliensSettingsSO : ScriptableObject
{
    [SerializeField]
    private AlienData[] alienData;

    public AlienView GetAlienView(string alienFamily)
    {
        if (alienData == null)
        {
            Debug.LogError("alienData is empty");
            return null;
        }

        var data = alienData.FirstOrDefault(x => x.alienFamily == alienFamily);

        if(data == null || data.alienView == null)
        {
            Debug.LogError("alienData is null or view is empty");
            return null;
        }

        return data.alienView;
    }
} 
