﻿using UnityEngine;

[CreateAssetMenu(fileName = "ViewsContainer", menuName = "Scriptables/ViewsContainer")]
public class ViewsContainerSO : ScriptableObject
{
    [SerializeField]
    private ProjectileView[] projectileViews;

    [SerializeField]
    private RayView[] rayViews;

    public ProjectileView GetProjectileView(int id)
    {
        if (projectileViews == null || id > projectileViews.Length)
        {
            Debug.LogError("projectileViews is out of range or empty");
            return null;
        }

        return projectileViews[id];
    }

    public RayView GetRayView(int id)
    {
        if (rayViews == null || id > rayViews.Length)
        {
            Debug.LogError("rayViews is out of range or empty");
            return null;
        }

        return rayViews[id];
    }
}
