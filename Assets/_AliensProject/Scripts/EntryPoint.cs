﻿using System.Linq;
using UnityEngine;

public class EntryPoint : MonoBehaviour
{
    [SerializeField] private AliensSettingsSO aliensSettings;
    [SerializeField] private ViewsContainerSO viewsContainer;

    [SerializeField] private GameSettings gameSettings;

    private Game game;

    public void Awake()
    {
        game = new Game(gameSettings, aliensSettings, viewsContainer);
    }

    private void Start()
    {
        game.StartGame();
    }

    private void Update()
    {
        game.Update(Time.deltaTime);
    }

    private void OnDestroy()
    {
        game.Dispose();
    }
}
