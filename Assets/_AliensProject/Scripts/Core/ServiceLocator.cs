using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

public interface IService
{
    void Initialize(ServiceLocator services);
}

public class ServiceLocator
{
    private readonly IDictionary<Type, IService> serviceInstances;

    private static ServiceLocator instance;

    public static ServiceLocator Instance
    {
        get
        {
            if (instance == null)
            {
                instance = new ServiceLocator();
            }
            return instance;
        }
    }

    internal ServiceLocator()
    {
        this.serviceInstances = new Dictionary<Type, IService>();
    }

    public T GetService<T>() where T : IService
    {
        if (this.serviceInstances.ContainsKey(typeof(T)))
        {
            return (T)this.serviceInstances[typeof(T)];
        }
        else
        {
            try
            {
                ConstructorInfo constructor = typeof(T).GetConstructor(new Type[0]);
                Debug.Assert(constructor != null, "Cannot find a suitable constructor for " + typeof(T));

                T service = (T)constructor.Invoke(null);
                service.Initialize(this);

                serviceInstances.Add(typeof(T), service);

                return service;
            }
            catch (KeyNotFoundException)
            {
                throw new ApplicationException("The requested service is not registered");
            }
        }
    }

    public void Register<T>(T serviceInstance) where T : IService
    {
        serviceInstances.TryAdd(typeof(T), serviceInstance);
        serviceInstance.Initialize(this);
    }

    public void Register<T>() where T : IService, new()
    {
        var service = new T();
        service.Initialize(this);
    }
}

public class ServiceA : IService
{
    public void Initialize(ServiceLocator services)
    {
        var b = services.GetService<ServiceB>();
    }
}

public class ServiceB : IService
{
    public void Initialize(ServiceLocator services)
    {
        var a = services.GetService<ServiceA>();
    }
}
