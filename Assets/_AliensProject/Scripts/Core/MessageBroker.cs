using System.Collections.Generic;
using System;

public class MessageBroker
{
    private static MessageBroker instance;
    private Dictionary<Type, List<Action<object>>> subscribers;

    private MessageBroker()
    {
        subscribers = new Dictionary<Type, List<Action<object>>>();
    }

    public static MessageBroker Instance
    {
        get
        {
            if (instance == null)
            {
                instance = new MessageBroker();
            }
            return instance;
        }
    }

    public void Subscribe<T>(Action<T> action)
    {
        Type messageType = typeof(T);

        if (!subscribers.ContainsKey(messageType))
        {
            subscribers[messageType] = new List<Action<object>>();
        }

        subscribers[messageType].Add(obj => action((T)obj));
    }

    public void Unsubscribe<T>(Action<T> action) 
    {
        Type messageType = typeof(T);

        if (subscribers.ContainsKey(messageType))
        {
            subscribers[messageType].Remove(obj => action((T)obj));

            if (subscribers[messageType].Count == 0)
            {
                subscribers.Remove(messageType);
            }
        }
    }

    public void Publish<T>(T message)
    {
        Type messageType = typeof(T);

        if (subscribers.ContainsKey(messageType))
        {
            foreach (var action in subscribers[messageType])
            {
                action.Invoke(message);
            }
        }
    }
}
