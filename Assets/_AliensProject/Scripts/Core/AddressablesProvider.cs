using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using Object = UnityEngine.Object;

namespace Project.Addressable
{
    public enum KeepObjectsType
    {
        DestroyOnClear,
        KeepInMemory
    }

    public class AddressablesProvider : IDisposable, IService
    {
        private Dictionary<KeepObjectsType, List<Object>> loadedInstances;
        private Dictionary<KeepObjectsType, Dictionary<object, AsyncOperationHandle>> loadedAssetHandlers;

        public AddressablesProvider()
        {
            loadedInstances = new Dictionary<KeepObjectsType, List<Object>>()
            {
                {KeepObjectsType.DestroyOnClear, new List<Object>()},
                {KeepObjectsType.KeepInMemory, new List<Object>()}
            };

            loadedAssetHandlers = new Dictionary<KeepObjectsType, Dictionary<object, AsyncOperationHandle>>()
            {
                { KeepObjectsType.DestroyOnClear, new Dictionary<object, AsyncOperationHandle>() },
                { KeepObjectsType.KeepInMemory, new Dictionary<object, AsyncOperationHandle>() }
            };
        }

        public void Initialize(ServiceLocator services)
        {
            
        }

        public void Dispose()
        {
            ForceClearAllMemory();
        }

        public async Task Initialize()
        {
            await Addressables.InitializeAsync().Task;
        }

        public void ClearMemory()
        {
            Debug.Log("Clear Addressable memory");
            loadedInstances[KeepObjectsType.DestroyOnClear].Clear();

            foreach (var item in loadedAssetHandlers[KeepObjectsType.DestroyOnClear])
            {
                if (item.Value.IsValid())
                {
                    try
                    {
                        Addressables.Release(item.Value);
                    }
                    catch (Exception ex)
                    {
                        Debug.Log(ex.ToString());
                    }
                }
            }
            loadedAssetHandlers[KeepObjectsType.DestroyOnClear].Clear();
        }

        public void ForceClearAllMemory()
        {
            ClearMemory();

            loadedInstances[KeepObjectsType.KeepInMemory].Clear();
            foreach (var item in loadedAssetHandlers[KeepObjectsType.KeepInMemory])
            {
                if (item.Value.IsValid())
                {
                    try
                    {
                        UnityEngine.AddressableAssets.Addressables.Release(item.Value);
                    }
                    catch (Exception ex)
                    {
                        Debug.Log(ex.ToString());
                    }
                }
            }

            loadedAssetHandlers[KeepObjectsType.KeepInMemory].Clear();
        }

        public IList<T> LoadAssetsByKey<T>(string keyOrLabel, KeepObjectsType keepType = KeepObjectsType.DestroyOnClear) where T : Object
        {
            try
            {
                if (loadedAssetHandlers[keepType].ContainsKey(keyOrLabel))
                {
                    return loadedAssetHandlers[keepType][keyOrLabel].Convert<IList<T>>().Result;
                }

                // Check if Address exists
                var locationsHandle = Addressables.LoadResourceLocationsAsync(keyOrLabel);

                if (locationsHandle.WaitForCompletion().Count == 0)
                {
                    Debug.Log($"Unable to load Asset with Address: {keyOrLabel}");
                    return null;
                }

                Addressables.ReleaseInstance(locationsHandle);

                var assetHandle = Addressables.LoadAssetsAsync<T>(keyOrLabel,
                    (loadedObj) =>
                    {
                        loadedInstances[keepType].Add(loadedObj);
                    });

                IList<T> loadedObjects = assetHandle.WaitForCompletion();

                loadedAssetHandlers[keepType].Add(keyOrLabel, assetHandle);

                return loadedObjects;
            }
            catch (System.Exception ex)
            {
                Debug.LogError($"{keyOrLabel} : {ex}");
                return null;
            }
        }

        public T LoadAssetByKey<T>(string key, KeepObjectsType keepType = KeepObjectsType.DestroyOnClear) where T : Object
        {
            try
            {
                if (loadedAssetHandlers[keepType].ContainsKey(key))
                {
                    var handle = loadedAssetHandlers[keepType][key].Convert<T>();

                    if (handle.IsDone == false)
                    {
                        return handle.WaitForCompletion();
                    }

                    return handle.Result;
                }

                // Check if Address exists
                var locationsHandle = UnityEngine.AddressableAssets.Addressables.LoadResourceLocationsAsync(key);

                if (locationsHandle.WaitForCompletion().Count == 0)
                {
                    Debug.Log($"Unable to load Asset with Address: {key}");
                    return null;
                }

                UnityEngine.AddressableAssets.Addressables.ReleaseInstance(locationsHandle);

                var assetHandle = UnityEngine.AddressableAssets.Addressables.LoadAssetAsync<T>(key);
                T obj = assetHandle.WaitForCompletion();
                loadedInstances[keepType].Add(obj);

                loadedAssetHandlers[keepType].Add(key, assetHandle);

                return obj;
            }
            catch (System.Exception ex)
            {
                Debug.LogError(ex.ToString());
                return null;
            }
        }

        public AsyncOperationHandle<T> LoadAssetByKeyAsync<T>(string key, KeepObjectsType keepType = KeepObjectsType.DestroyOnClear) where T : Object
        {
            try
            {
                if (loadedAssetHandlers[keepType].ContainsKey(key))
                {
                    return loadedAssetHandlers[keepType][key].Convert<T>();
                }

                var locationsHandle = UnityEngine.AddressableAssets.Addressables.LoadResourceLocationsAsync(key);
                var resourcesList = locationsHandle.WaitForCompletion();

                if (resourcesList.Count > 0)
                {
                    UnityEngine.AddressableAssets.Addressables.Release(locationsHandle);

                    var loadAssetHandle = UnityEngine.AddressableAssets.Addressables.LoadAssetAsync<T>(key);

                    loadedAssetHandlers[keepType].Add(key, loadAssetHandle);

                    loadAssetHandle.Completed += (result) =>
                    {
                        loadedInstances[keepType].Add(result.Result);
                    };
                    return loadAssetHandle;
                }
                else
                {
                    UnityEngine.AddressableAssets.Addressables.Release(locationsHandle);

                    Debug.Log($"Unable to load Asset with Address: {key}");
                    return default;
                }
            }
            catch (System.Exception ex)
            {
                Debug.LogError(ex);
                return default;
            }
        }

        public AsyncOperationHandle<T> LoadAssetByKeyAsyncNotSafe<T>(string key, KeepObjectsType keepType = KeepObjectsType.DestroyOnClear) where T : Object
        {
            try
            {
                if (loadedAssetHandlers[keepType].ContainsKey(key))
                {
                    return loadedAssetHandlers[keepType][key].Convert<T>();
                }

                var loadAssetHandle = UnityEngine.AddressableAssets.Addressables.LoadAssetAsync<T>(key);

                loadedAssetHandlers[keepType].Add(key, loadAssetHandle);

                loadAssetHandle.Completed += (result) =>
                {
                    loadedInstances[keepType].Add(result.Result);
                };

                return loadAssetHandle;
            }
            catch (System.Exception ex)
            {
                Debug.LogError(ex);
                return default;
            }
        }

        public void LoadAssetsByKeyAsyncNotSafe<T>(string keyOrLabel, KeepObjectsType keepType = KeepObjectsType.DestroyOnClear) where T : Object
        {
            try
            {
                if (loadedAssetHandlers[keepType].ContainsKey(keyOrLabel))
                {
                    return;
                }

                var assetHandle = Addressables.LoadAssetsAsync<T>(keyOrLabel,
                                                                  (loadedObj) =>
                                                                  {
                                                                      loadedInstances[keepType].Add(loadedObj);
                                                                  });

                loadedAssetHandlers[keepType].Add(keyOrLabel, assetHandle);
            }
            catch (System.Exception ex)
            {
                Debug.LogError($"{keyOrLabel} : {ex}");
            }
        }

        public async Task LoadSceneByKeyAsync(string key)
        {
            var sceneHandle = Addressables.LoadSceneAsync(key);
            await sceneHandle.Task;
        }

        public void ReleaseHandle<T>(AsyncOperationHandle<T> handle) where T : Object
        {
            if (handle.IsValid() == false)
            {
                return;
            }

            foreach (var list in loadedInstances.Values)
            {
                if (list.Contains(handle.Result))
                {
                    list.Remove(handle.Result);
                    break;
                }
            }

            foreach (var dict in loadedAssetHandlers.Values)
            {
                foreach (var keypair in dict)
                {
                    if (object.Equals(keypair.Value.Result, handle.Result))
                    {
                        dict.Remove(keypair.Key);
                        break;
                    }
                }
            }

            Addressables.Release(handle);
        }

        public T LoadAssetByReference<T>(AssetReference assetReference) where T : Object
        {
            var keepType = KeepObjectsType.DestroyOnClear;
            var key = (string)assetReference.RuntimeKey;

            try
            {
                if (loadedAssetHandlers[keepType].ContainsKey(key))
                {
                    return loadedAssetHandlers[keepType][key].Convert<T>().Result;
                }

                var assetHandle = Addressables.LoadAssetAsync<T>(key);
                T obj = assetHandle.WaitForCompletion();
                loadedInstances[keepType].Add(obj);

                loadedAssetHandlers[keepType].Add(key, assetHandle);

                return obj;
            }
            catch (System.Exception ex)
            {
                Debug.LogError(ex.ToString());
                return null;
            }
        }
    }
}
